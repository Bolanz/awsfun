import boto3
import boto

# Create an S3 client
s3 = boto3.client('s3')

# Call S3 to list current buckets
response = s3.list_buckets()
s3=boto.connect_s3()
# Get a list of all bucket names from the response
buckets = [bucket['Name'] for bucket in response['Buckets']]
total=0
# Print out the bucket list
print("Bucket List: %s" % buckets)
for bu in buckets:
    bu1=boto3.resource('s3').Bucket(bu)
    bu_total=0
#    print(bu)
    for key in bu1.objects.all():
       print(key)
       bu_total+=key.size
       print(bu_total)
    total+=bu_total
print ("Total in KB")
print (total/1024)
print ("Total in MB")
print (total/1024/1024)